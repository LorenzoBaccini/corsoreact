# App del corso react ariestech

[![NPM Version][npm-image]][npm-url]
[![NPM Downloads][downloads-image]][downloads-url]
[![Node.js Version][node-version-image]][node-version-url]
[![Build Status][travis-image]][travis-url]
[![Test Coverage][coveralls-image]][coveralls-url]

Progetto creato per il corso

## Installation 

Per installare questo pacchetto bisogna andare su [NPM](https://npmjs.org) ed eseguire il codice

```sh
$ npm install primo-progetto
```

